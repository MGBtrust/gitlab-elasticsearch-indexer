module gitlab.com/gitlab-org/gitlab-elasticsearch-indexer

go 1.16

require (
	github.com/aws/aws-sdk-go v1.38.35
	github.com/deoxxa/aws_signing_client v0.0.0-20161109131055-c20ee106809e
	github.com/go-enry/go-enry/v2 v2.7.1
	github.com/mailru/easyjson v0.0.0-20190403194419-1ea4449da983 // indirect
	github.com/olivere/elastic v6.2.24+incompatible
	github.com/sirupsen/logrus v1.8.1
	github.com/stretchr/testify v1.7.0
	gitlab.com/gitlab-org/gitaly/v14 v14.4.2
	gitlab.com/gitlab-org/labkit v1.5.0
	gitlab.com/lupine/icu v1.0.0
	golang.org/x/net v0.0.0-20210505214959-0714010a04ed
	golang.org/x/tools v0.1.0
	google.golang.org/grpc v1.38.0
)
